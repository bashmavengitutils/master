#!/bin/bash -e
#
# Copyright (C) 2019 Tad Marko
#

die () {
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 1 ] || die "Usage: tag_all.sh version_string"

projects=$(grep "<module>" pom.xml | sed -re "s/<module>..\/(.*)<\/module>/\1/g" | sort | xargs echo)
projects+=('master')

#
# Loop over each project, tagging and pushing the tag to the server.
# Tread lightly!
#
cd ..
for p in $projects
do
	cd $p
	git tag -a $1 -m "Release $1"
	git push origin $1
	echo
	cd ..
done

# If there are no errors, finish up by tagging master.

cd master
git tag -a $1 -m "Release $1"
git push origin $1
