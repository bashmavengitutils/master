#!/bin/bash
#
# Copyright (C) 2019 Tad Marko
#
# Extracts modules from a pom.xml file located in the current directory and
# compares the commit SHA1 of the PROD branch, or the specified branch, to
# that of origin/master.
#
# While running, the script will print if the branch matches or differs for
# the current project being iterated, and will print a summary of non-matching
# projects before exiting.
#

function max ()
{
    if [ $1 -gt $2 ]
    then
        echo $1
    else
        echo $2
    fi
}

BLACK="\e[0;30m"
BLUE="\e[0;34m"
GREEN="\e[0;32m"
CYAN="\e[0;36m"
RED="\e[0;31m"
PURPLE="\e[0;35m"
BROWN="\e[0;33m"
GRAY="\e[0;37m"
DARK_GRAY="\e[1;30m"
LIGHT_BLUE="\e[1;34m"
LIGHT_GREEN="\e[1;32m"
LIGHT_CYAN="\e[1;36m"
LIGHT_RED="\e[1;31m"
LIGHT_PURPLE="\e[1;35m"
YELLOW="\e[1;33m"
WHITE="\e[1;37m"
NORMAL="\e[0;0m"

OM='origin/master'

fetch=false
noerr=false
brancherrseen=false
branch='PROD'
count=0

OPTS=$(getopt -o b:fhs --long branch:,fetch,help -- "$@")
[ $? -eq 0 ] || { 
    echo 'Incorrect options provided'
    echo 'Usage: checkbranchstatus.sh [-f|--fetch -h|--help -s -b|--branch branchname]'
    exit 1
}
eval set -- "$OPTS"

while true
do
    case $1 in
	-b|--branch)
	    shift
	    branch=$1
	    ;;
	-h|--help)
	    echo
	    echo 'Extracts module names from pom.xml found in the current directory.'
	    echo 'It is assumed that these modules all exist in directories that are'
	    echo 'peers to the current directory.'
	    echo
	    echo 'Each directory is visited in turn and the SHA1 commit tag of the'
	    echo 'local PROD branch is compared to that of origin/master. Differences'
	    echo 'are noted as they are found and a summary displayed at the end of'
	    echo 'processing.'
	    echo
	    echo 'Usage: checkPRODstatus.sh [-f|--fetch -h|--help -t|--tag branchname]'
	    echo 
	    echo '-b or --branch branchname will compare the specified branch instead of the default PROD'
	    echo '-f or --fetch will call git --fetch for each repository.'
	    echo '-h or --help displays this help.'
	    echo
	    exit 0
	    ;;
	-f|--fetch)
	    fetch=true
	    ;;
	-s)
	    noerr=true
	    echo 'Suppress errors for projects that don''t have the specified branch.'
	    ;;
	--)
	    shift
	    break
	    ;;
    esac
    shift
done

projects=$(grep "<module>" pom.xml | sed -re "s/<module>..\/(.*)<\/module>/\1/g" | sort | xargs echo)

branchNameLen=${#branch}
omLen=${#OM}
nameLen=$(max $omLen $branchNameLen)

branchIntro=$(printf '%-*s SHA1 is:' $nameLen $branch)
omIntro=$(printf '%-*s SHA1 is:' $nameLen $OM)

declare -a BranchNotOnMaster
declare -a ErrorProjects

cd ..
for p in $projects 
do
    cd $p
    echo -e "${CYAN}$p${NORMAL}"
    if $fetch; then
	echo 'Executing git fetch...'
	git fetch
	if [[ $? -ne 0 ]] ; then
	    echo -e "${LIGHT_RED}Error fetching for project ${p}. Aborting.${NORMAL}"
  	    exit 1
        fi
    fi
    prodrev=$(git rev-parse --short $branch)
    if [[ $? -ne 0 ]]
    then	
	if ! $noerr; then
	    echo -e "${RED}Cannot get SHA1 for ${p}:${branch}. Branch does not exist?${NORMAL}"
	    ErrorProjects+=($p)
	else
	    brancherrseen=true
	fi
	cd ..
	continue
    else
	((count++))
    fi
    masterrev=$(git rev-parse --short origin/master)
    echo "$branchIntro $prodrev"
    echo "$omIntro $masterrev"
    if [ "$prodrev" == "$masterrev" ]
    then
        echo -e "${GREEN}They are the same${NORMAL}"
    else
        echo -e "${YELLOW}They DIFFER${NORMAL}"
        BranchNotOnMaster+=($p)
    fi
    cd ..
    echo
done

echo

if [ ${#ErrorProjects[@]} -gt 0 ]
then
    echo -e "${LIGHT_RED}An error was encountered for these projects:${RED}"
    echo
    for i in "${ErrorProjects[@]}"
    do
        echo "${i}"
    done
    echo -e "${NORMAL}"
    exit 1
fi

if [ ${#BranchNotOnMaster[@]} -gt 0 ]
then
    echo "${branch} is not the same commit as origin/master for these projects:"
    echo
    for i in "${BranchNotOnMaster[@]}"
    do
	echo "${i}"
    done
else
    if $brancherrseen
    then
	echo -e "${LIGHT_BLUE}${branch} ${LIGHT_GREEN}is on master for ${count} projects where it was found.${NORMAL}"
    else
	echo -e "${LIGHT_BLUE}${branch} ${LIGHT_GREEN}is on master for all ${count} projects.${NORMAL}"
    fi
    echo
fi
