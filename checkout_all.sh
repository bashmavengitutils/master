#!/bin/bash -e
#
# Copyright (C) 2019 Tad Marko
#

# TODO: Add namespace default and as paramter.

projects=$(grep "<module>" pom.xml | sed -re "s/<module>..\/(.*)<\/module>/\1/g" | sort | xargs echo)

cd ..
for p in $projects 
do
    git clone -b PROD git@labserv02:$p.git
done
