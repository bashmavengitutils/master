# Master

The master project for the bash utils I use to manage a specific maven build
process.

## Description

I do not suggest that these utilities represent any sort of best practices, or
even that they represent good, decent, or even not very bad practices. These
utilities were written to deal with a large group of closely related, but
separate, Java projects that are built with Maven from a single master project.
These projects had been stored in CVS, as the team was small and we elected to
skip the whole SVN thing.

Eventually it became somewhat necessary to move to git as some of the tools we
were using were well integrated with git only. I did not want to have a mixture
of RCS systems, so we decided to switch everything to git once I learned that
git cvsimport works well (enough).

Learning git has presented enough of a learning curve on its own, so I have not
changed any of our processes or tried to rearrange our projects at this time.
I may eventually do so once I feel I have reached a sufficient level of 
proficiency with git.

Even then, some of these scripts might be useful and I offer them here for
informational purposes only. Feel free to make use of these and to make
suggestions, but please understand the history behind them before offering any
criticisms.

Tad

## Scripts

These scripts work by simplistically parsing out the module names for the sub
projects found in the pom.xml in the current directory. It is assumed these as
the same as each project's directory name and that these directories are
located as peers to the current directory. Each script does a `cd ../modulename`
in a loop and runs certain git commands in each module's directory.

##### checkbranchstatus.sh
Compares the head commit of a branch to origin/master and notes if they are 
different. The default branch is PROD, but a different one may be specified
with the -b option. Supplying the -f option will perform a fetch before the
comparison.

##### checkout_all.sh
Performs a git checkout for each module. This is helpful when setting up a new
workspace.

##### findmodified.sh
Simply displays a short summary of any commit within the past two weeks for
each module.

##### tag_all.sh
Attaches the supplied tag to each module.

##### update_all.sh
Performes git pull for each module.

## To Do

* Create child projects
* Modify these scripts to pull out some common code
